# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Прежний функционал с петлёй | https://yadi.sk/i/V7s1gCK-PXtxAQ |
| Функционал с аргументами | https://yadi.sk/i/f_mwaOFkY1KWkw | 
| Новый функционал info с аргументом | https://yadi.sk/i/TlUAteNn-HZ2FA |